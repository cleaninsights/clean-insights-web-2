Clean Insights
====================

This is the website for https://cleaninsights.org

Agency Jekyll theme
====================

Agency theme based on [Agency bootstrap theme](https://startbootstrap.com/template-overviews/agency/).


## Development

### MacOS

Current Jekyll doesn't work with Ruby before 2.5.0 and it also doesn't work with Big Sur's 
built-in Ruby 2.6.3, because the dependency `eventmachine` can't compile.

So just use [rbenv](https://github.com/rbenv/rbenv):

```sh
brew install rbenv
rbenv init
# Follow instructions printed by that command.

#Check setup:
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor | bash

rbenv install 3.0.3
```


### Common

You don't need to install Jekyll on your local system, but use the project-local 
Jekyll dev server like this: 

```sh
bundler install
bundler exec jekyll serve
```
