---
layout: page
permalink: /event-schedule
---

# Symposium Extraordinaire Schedule

<a href="https://archive.org/details/cise-trailer-1-final" target="_new"><img src="/assets/images/trailer.jpg"/></a>

![header](/assets/images/section-head.png)

## Week of May 11 - The Parade: Welcoming Act
### Monday
* [Welcome to the Extraordinaire! Podcast](https://guardianproject.info/podcast/2020/cleaninsights-welcome-to-the-extraordinaire.html)
[![organizers](/assets/images/ciseorganizers.png)](https://guardianproject.info/podcast/2020/cleaninsights-welcome-to-the-extraordinaire.html)

### Tuesday
Intro to the project (video and podcast), welcome emails, discussion space
* [Listen to the Clean Insights origin story](https://guardianproject.info/podcast/2020/episode7-cleaninsights.html) on the Guardian Project's podcast
* Join our [CleanInsights Discussion Room](https://riot.im/app/#/room/#cleaninsights-discussion:matrix.org)
	* join via any Matrix client: #cleaninsights-discussion:matrix.org
	* See a [read-only view of the discussion](https://view.matrix.org/room/!gawonsIgQvFMKiMyYX:matrix.org/)
* Follow the [#cleaninsights](https://twitter.com/search?q=%23cleaninsights&src=typed_query) hashtag on the fediverse (twitter, mastadon, status, etc)
* Read the Announce List emails: [Register](/register)

### Wednesday
* Podcast Panel with Arturo Filastò of the Open Observatory of Network Interference [OONI](https://ooni.org/) on "Measuring the Hidden Depths of the Internet"
	* **Now Available:** [Listen to the podcast panel!](https://guardianproject.info/podcast/2020/cleaninsights-panel-ooni.html)

[![Arturo and Nathan](https://guardianproject.info/podcast/images/posts/cise-podcast2-ooni-participants.jpg)](https://guardianproject.info/podcast/2020/cleaninsights-panel-ooni.html)

### Thursday
* [Activity 1: Make Your Own Dear Data Postcard](/event-activity-dear-data)

[![dear data postcard](/assets/images/dear-data.jpg)](/event-activity-dear-data)

### Friday
* Participant Spotlight
	* Experience: The Amazing "Informed Consent Pop Quiz" of [OONI Probe for Android and iOS](https://ooni.org/install/mobile)
* Live Work Session
	* Live Work Session on the Android SDK - [YouTube Recording](https://youtu.be/Rz2knS--68Y?t=1244) [Archive.org](https://archive.org/details/cleaninsights-live-work-1)
	[![Nathan work session](/assets/images/liveworksession1image.jpg)](https://youtu.be/Rz2knS--68Y?t=1244)
	[![notes work session](/assets/images/liveworksession1notes_thumb.jpg)](/assets/images/liveworksession1notes.jpg)

![header](/assets/images/section-head02.png)

## Week of May 18 - Rolla Bola and Juggling
### Tuesday
* Group kickoff:  Jitsi Meet, email and chat for live discussions on our three track topics (Data, Design and Developers)
![the live welcome](/assets/images/livewelcomeMay19.jpg)

### Wednesday
* "Do the Dashboard Collage!!" activity: [Available on Okthanks.com](https://okthanks.com/blog/2020/5/12/q6148amwc089vvcj56ixwc7jyd82f1)
![data dashboard circulo](/assets/images/datadash_circulo.jpg)

* [Podcast Panel Discussion on "Ethics in Computer Science"](https://guardianproject.info/podcast/2020/cleaninsights-ethics-in-compsci.html): 
	* Professor James Mickens, Harvard University and Dr. Gina Helrich, Internews
[![ethics podcast headhosts](/assets/images/cise-podcast-mickens.jpg)](https://guardianproject.info/podcast/2020/cleaninsights-ethics-in-compsci.html)

### Thursday

### Friday
* LIVE  10am EST (1hr) [**register to receive the link**](/register)
	* 30min Workout 🕴Led by a real NYC Lion Tamer
	![Free Your Mind](/assets/images/may22livefreeyourmind2.jpg)

	* 30min Draw with Me 🎨✏ Led by Okthanks owner, Carrie Winfrey
	Watch now on [Internet Archive](https://archive.org/details/drawdataiwthme_20200526) or [YouTube](https://www.youtube.com/watch?v=3D6DyUK5xbU&feature=youtu.be)
	[![draw with me](/assets/images/livedrawdatawithme.jpg)](https://archive.org/details/drawdataiwthme_20200526)



![header](/assets/images/section-head.png)

## Week of May 25 - The Tightrope Act
### Monday
* HOLIDAY - Rest up everyone!

### Tuesday
* Group discussion: “Boos and Cheers: Feedback so far!” (Jitsi Meet, email and chat)

### Wednesday
* Activity 3: ["Walking the Consent Tightrope"](https://okthanks.com/blog/2020/5/26/walking-the-tightrope)
	[![activity 3 image](/assets/images/activity3tightrope.jpg)](https://okthanks.com/blog/2020/5/26/walking-the-tightrope)

### Friday
* Podcast Keynote: Nathan Freitas on “Modeling the Threats of Measurement”.
[![nathan talks on threat modeling](/assets/images/talk-threatmodeling.jpg)](https://www.youtube.com/watch?v=nt6ac3WoogA&feature=youtu.be)

	*  [Listen now](https://guardianproject.info/podcast/2020/cleaninsights-threatmodeling.html) or subscribe via [Apple Podcasts](https://podcasts.apple.com/us/podcast/en-garde-the-guardian-project-podcast/id1505562297) or [RSS](https://guardianproject.info/podcast/podcast.xml)
	* [Download and view the PDF presentation](https://cleaninsights.org/assets/docs/CleanInsights-ThreatModelingForMeasurementandAnalytics.pdf) that accompanies this podcast talk.
	* You can watch a video of this talk with slides on [YouTube](https://www.youtube.com/watch?v=nt6ac3WoogA&feature=youtu.be) or the [Internet Archive](https://archive.org/details/clean-insights-threat-modeling-for-measurement)

* Participant Spotlight "Draw Data with Me" Featurnig QubesOS! (video coming soon!)
* Live Work Session on Proposed Development Tools - 2pm UTC/10am ET/9am CT (video coming soon!)

![header](/assets/images/section-head02.png)

## Week of June 1 - Human Pyramid (The Grand Finale)
### Tuesday
* Group discussion: “All in Together” (Jitsi Meet, email and chat) -  2pm UTC/10am ET/9am CT

* Work Session: "Draw Data with Me: Qubes OS" - watch now on [YouTube](https://www.youtube.com/watch?v=-UEv8hL-doI) or the [Internet Archive](https://archive.org/details/draw-data-with-qubes-no-sound)

[![qubes images](/assets/images/drawdatawithqubes.jpg)](https://www.youtube.com/watch?v=-UEv8hL-doI)

### Wednesday
* Work Session: "Draw Data with Me: Circulo App" - learn more at [https://encirculo.org](https://encriculo.org) 
![circulo session](/assets/images/drawwithme-circulo.jpg)

### Friday
* Research Report by Okthanks: ["Learning from Open-Source Tool Teams about Privacy Preserving Measurement"](/assets/docs/CI User Research Report, April 2020-Public.pdf)
[![research report](/assets/images/researchparticipants.jpg)](/assets/docs/CI User Research Report, April 2020-Public.pdf)

![header](/assets/images/section-head02.png)

## Week of June 8 - Le Grande Finale!
### Tuesday
* Live Work Session on Proposed Development Tools - 2pm UTC/10am ET/9am CT

### Thursday
* Work Session: "How to Measure Círculo, an app built on Safety, Security, and Privacy?"
	* Watch 2 minute video on [YouTube](https://www.youtube.com/watch?v=bUoRfbWxcwM&feature=youtu.be) or the [Internet Archive](https://archive.org/details/circulo-draw-data-with-me-final)
	*  [Listen to the episode now](https://guardianproject.info/podcast/2020/cleaninsights-circulo.html) or subscribe via [Apple Podcasts](https://podcasts.apple.com/us/podcast/en-garde-the-guardian-project-podcast/id1505562297) or [RSS](https://guardianproject.info/podcast/podcast.xml)

### Friday
* Wrap-up Talk and Grand Finale! 2pm UTC/10am ET/9am CT (register for live invite link)
	* “Next steps for Clean Insights as an SDK”
	* Discussion: "Can Analytics Be Ethical? Are you ready to measure more meaningfully?"
