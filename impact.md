---
layout: page
title: Impact Reports
description: "Impact Reports"
permalink: /impact
---

# IMPACT REPORTS

Below are series of reports covering our work with partners to add Clean Insights into their application or service. Each report covers consideration of what and how to measure, definition of the desired insights, the consent user experience and learning and outcomes from the process.

[![Mailvelope Impact Report](assets/images/Mailvelope__1_.png)](assets/docs/Mailvelope__Clean_Insights_Impact_Report.pdf)

[Read "Mailvelope" Impact Report](assets/docs/Mailvelope__Clean_Insights_Impact_Report.pdf)

<hr/>

[![Convene Impact Report](assets/images/ConveneImpact.png)](assets/docs/Convene_Clean_Insights_Impact_Report_March2022.pdf)

[Read "Convene" Impact Report](assets/docs/Convene_Clean_Insights_Impact_Report_March2022.pdf)

[![WeClock Impact Report](assets/images/WeClockImpact.png)](assets/docs/WeClock_Clean_Insights_Impact_Report.pdf)

[Read "WeClock" Impact Report](assets/docs/WeClock_Clean_Insights_Impact_Report.pdf)

[![Tella Implementation Report](assets/images/tella_impact_report.png)](assets/docs/Tella_Clean_Insights_Report_Spring2022.pdf)

[Read "Tella" Implementation Report](assets/docs/Tella_Clean_Insights_Report_Spring2022.pdf)

Now that you see how it can be done, take the next step and [get started with clean insights](/getstarted)
---
