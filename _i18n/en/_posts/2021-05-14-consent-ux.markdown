---
title: A Guide to Clean Consent UX
subtitle: Embrace transparency and user control when integrating analytics
layout: default
modal-id: 11
date: 2021-05-14
img: consentux.png
thumbnail: consentux.png
alt: image-alt
project-date: Feb 2022
category: Case Study
link: https://okthanks.com/blog/2021/5/14/clean-consent-ux
description: Embrace transparency and user control when integrating analytics! The way it’s perceived will greatly affect the success of your app and happiness of your users. The consent models we’ve developed as part of Clean Insights are a first step into creating an experience that embraces these values while minimizing emotional friction and annoyance within the app.<br/><br/><a href="https://okthanks.com/blog/2021/5/14/clean-consent-ux">read more about Clean Consent UX</a> 
---
