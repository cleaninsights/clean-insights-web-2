---
title: Research Report
subtitle: Learning from Open-Source Tool Teams about Privacy Preserving Measurement
layout: default
modal-id: 3
date: 2014-07-16
img: researchparticipants.jpg
thumbnail: researchparticipants.thumb.jpg
alt: image-alt
project-date: April 2014
client: Start Bootstrap
category: Web Development
link: https://cleaninsights.org/assets/docs/CI%20User%20Research%20Report,%20April%202020-Public.pdf
description: Clean Insights is both a methodology and an open-source SDK for privacy-preserving measurement. It aims to help answer key questions about app usage patterns, while not enabling invasive surveillance of all user habits. Clean Insights gives developers a way to plug into a secure, private measurement platform and provides user interactions that are empowering rather than alienating.<br/><br/>The Clean Insights work grew out of the need for open-source teams to have a better understanding of how their apps are used and the impact they have. Its immediate focus is to provide a solution for small-medium sized open source teams. This report represents the user research that was done to understand the needs of various open-source teams.<br/><br/>We held interviews with the following 10 tool teams who are part of the BASICS project led byInternews. They include TunnelBear, OONI, Least Authority, Qubes OS, KeePassXC, MISP,CalyxOS, Umbrella App, Psiphon, and Tella.<br/><br/><a href="https://cleaninsights.org/assets/docs/CI%20User%20Research%20Report,%20April%202020-Public.pdf" target=_new><b>READ THE FULL REPORT</b></a>
---
