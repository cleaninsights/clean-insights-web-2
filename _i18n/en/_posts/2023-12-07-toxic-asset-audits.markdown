---
title: Toxic Asset Audits
subtitle: "Free: Find out what toxic data you're unknowingly collecting"
layout: default
modal-id: 11
date: 2023-12-07
img: toxic.png
thumbnail: toxic.thumb.png
alt: image-alt
project-date: Dec 2023
category: Services
link: /toxic
description: What toxic assets and data are you collecting, that you haven’t even thought of, that you are aggregating over time, building up toxicity, exposing your users to increased risk?!  Find out for free.
---
