---
title: Process - The Dashboard Collage
subtitle: Consider what measurement to collect and assess their usefulness when displayed on a dashboard.
layout: default
modal-id: 6 
date: 2020-05-12
img: dashboard.jpg
thumbnail: dashboard.thumb.jpg
alt: image-alt
project-date: May 2020
client: Clean Insights
category: Design
link: https://okthanks.com/blog/2020/5/12/q6148amwc089vvcj56ixwc7jyd82f1
description: With this activity, we are going to play out the full scenario of collecting measurements and assessing their usefulness when displayed on a dashboard. It provides a process for thinking critically about what questions you want answers to, imagining your dashboard narrative, determining which metrics you need to gather to tell that narrative and ultimately, getting you one step closer to understanding what is essential to improve your product. Watch the intro video to get started!<br/><br/><a href="https://okthanks.com/blog/2020/5/12/q6148amwc089vvcj56ixwc7jyd82f1" target=_new><b>DO THE DASHBOARD COLLAGE</b></a>
---
