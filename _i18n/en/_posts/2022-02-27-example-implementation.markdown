---
title: Example Implementation
subtitle: See a real-world implementation where we use the JS SDK in Mailvelope
layout: default
modal-id: 12
date: 2022-02-27
img: mailvelopepr.png
thumbnail: mailvelopepr.thumb.png
alt: mailvelope logo
project-date: February 2022
category: Case Study
link: https://github.com/mailvelope/mailvelope/commit/cdf4835631cc3094e63658f957247b93a85e419d
description: Wondering what a real world implementation looks like?  Here's the Pull Request for Mailvelope's measurement.
---
