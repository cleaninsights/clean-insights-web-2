---
title: Mailvelope Case Study
subtitle: Helping Mailvelope understand which webmail providers are most popular
layout: default
modal-id: 13
date: 2022-02-28
img: mailvelope.jpg
thumbnail: mailvelope.thumb.jpg
alt: image-alt
project-date: Feb 2022
category: Case Study
link: https://guardianproject.info/2022/02/28/privacy-preserving-analytics-in-the-real-world-mailvelope-case-study/
description: We love Mailvelope. It’s a popular browser extension for encrypting email messages. Now, Clean Insights is helping Mailvelope understand which webmail providers are most popular with their users so they can prioritize their development efforts.<br/><br/>  Anyone who has written software knows it takes hard work to craft a great user experience. That’s even more challenging in Mailvelope’s case. Their browser extension integrates with more than a dozen ever-changing third party webmail interfaces. The Mailvelope team asks itself questions like, &qout;Is time better spent improving the GMail integration or the mailbox.org one?&quot; The answer often hinges on which providers are most popular among Mailvelope users, information not yet readily available to the Mailvelope team.<br/><br/> So, Mailvelope asked us to set up the Clean Insights JavaScript SDK to measure which providers are most popular while respecting users’ consent and preserving their privacy. In drastic contrast to the web analytics norms, we only collect what we need in order to make important decisions.<br/><br/><a href="https://guardianproject.info/2022/02/28/privacy-preserving-analytics-in-the-real-world-mailvelope-case-study/">Read the full case study</a>
---
