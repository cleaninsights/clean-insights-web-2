---
layout: page
title: Clean Insights at the 2024 Global Gathering
description: "Let's get insightful IRL!"
permalink: /gg
---

# GLITTER SPONSOR of the Global Gathering

“The annual, in-person gathering attracts over 1,200+ digital rights defenders, 800+ organizations from 144+ countries. One of the most popular and loved events for pioneers working on issues sitting at the intersection of human rights and technology.”

[Learn more](https://www.digitalrights.community/blog/announcing-team-communitys-global-gathering)
or you can [access the Community Wiki](https://wiki.digitalrights.community/index.php?title=Global_Gathering_2024)

#### Come drink tea with us!

Clean Insights by Guardian Project is a new approach to measurement and analytics that focuses on consent, privacy, and security. It helps you answer key questions about the people you are trying to benefit, while avoiding invasive surveillance of their personal data and interactions. Just like the feeling of an “ah ha moment” after a delicious cup of fresh brewed tea, Clean Insights inspires and empowers, without all the toxic side effects. 

How do you know if your product is fulfilling its purpose? Satisfied with your insights but concerned about sharing them with Big Tech? Come meet the Clean Insights team! Look for someone in an [Insightful Tea[(/tea) shirt, and let's have a quick 5-minute chat to steer you in the right direction.

#### We will be doing in-person Toxic Asset Audits...

You may think you’ve done a good job designing your app in a privacy minded way. You may have passed a security audit for some of your source code. But what toxic assets and data are you collecting, that you haven’t even thought of, that you are aggregating over time, building up toxicity, exposing your users to increased risk?! These might be hidden in cookies, in your cloud storage, in third-party analytics, in data-cached client-side in your app, in monitoring metrics that identify people through PII (personally identifiable information)... these are all TOXIC DATA ASSETS!
<div style="width:100%;height:0px;position:relative;padding-bottom:56.25%;"><iframe src="https://streamyard.com/e/p43tfuchrbf8" width="100%" height="100%" frameborder="0" title=Embed recording allowfullscreen style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden;"></iframe></div>

<br/><br/>
<center><a href="https://calendly.com/meetjohn/toxic-asset-audit" class="btn btn-default taa-btn">SCHEDULE</a></center>

## [Contact us now for your FREE TOXIC ASSET AUDIT!](mailto:support@guardianproject.info?subject=toxicassetaudit) ##

We are here — the Clean Insights team, to help you identify and dispose of these toxic assets through our full-proof, one-day, toxic asset audit (TA^2). Our crack squad of toxic assessors will work with you to index, delineate, analyze and document all of your toxic assets and make a clean-up plan, so you can move forward in an insightful and fresh way. Don’t wait, start now, our first 10 audits are totally free!

<a href="mailto:support@guardianproject.info?subject=toxicassetaudit">We are ready to DETOXIFY you!</a>

---

Otherwise... learn how to [GET STARTED](/getstarted) with implementing Clean Insights into your app, service or device.

