---
layout: page
title: Getting started
description: "Take the first step torwards clean insights"
permalink: /getstarted
---

# Get Started

### *You’re one step closer to having critical insights to help you improve and measure your impact! Congratulations.*

Clean Insights Ingredients:
* Software Development Kits (Available for [Android](), [iOS](), [Javascript]() and [Rust]())
* Matomo analytics server instance
* Consent UX/UI

Clean Insights provides a full analytics solution for your app, with the help of a few required pieces. Probes from the SDK will record measurements within your app. That data is then sent in cycles to Matomo for further aggregation and visualization. Measurements are made possible by an engaged consent from your users. Let’s get going! Below, we’ve outlined step-by-step instructions to help you plan your integration.

## Step 1: Decide what you want to know
Clean Insights gives you the power to measure impact, learn where users spend time, run health checks and survey user satisfaction. Deciding what you want to know before collecting data takes some time and brain power, but it will be worth it. Here are some resources to help:

* [The ‘Dashboard Collage’ Activity](https://okthanks.com/blog/2020/5/12/q6148amwc089vvcj56ixwc7jyd82f1)
* [Consent User Experience Prototypes](https://okthanks.com/blog/2021/5/14/clean-consent-ux)

## Step 2: Define what you need to measure
When your developers start working with the SDK, they will set up a ‘campaign’ in the code. A ‘campaign’ is composed of the following parameters:
Which data is collected

* When to start and end a measurement cycle
* The length of a cycle; and
* The number of cycles

*Note: The Clean Insights methodology refers to data collected as ‘measurements’ to emphasize its privacy-preserving strategy.*

## Step 3: Choose your consent experience
Your consent experience involves how you ask users for permission to measure, along with the full cycle of their experience while measurements are enabled. This includes how users can opt out, how they are aware or unaware that measurements are enabled, and what control users have.

* Refer to the [Consent UX Prototypes](https://okthanks.com/blog/2021/5/14/clean-consent-ux) for inspiration.

## Step 4: Integrate
Dig into the code on [Gitlab](https://gitlab.com/cleaninsights) for full instructions. View code.

# We’re ready to support you
### *Get a design and strategy consult, developer support or free hosting.*

## [Contact us for a free consult](mailto:support@guardianproject.info?subject=cleaninsights%20consult)
